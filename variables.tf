#####################
# --- Variables --- #
#####################

variable "region" {
  type        = string
  description = "The region into which the Terraform Configuration is being deployed."
}

variable "resource_name_prefix" {
  type        = string
  description = "The name to prefix to all created resources."
}

variable "domain_name" {
  type        = string
  description = "Domain name of the website/service"
}

variable "cloudwatch_logs_retention_days_default" {
  type        = string
  description = "The amount of days to keep cloudwatch logs by default"
}

variable "environment" {
  type        = string
  description = "The deployment environment name"
}