#####################
# --- Variables --- #
#####################

variable "bucket_name" {
  description = "The name to assign to the S3 bucket hosting the static site files"
  type        = string
}

variable "bucket_versioning_enabled" {
  description = "Whether or not to enable bucket versioning"
  type        = string
  default     = "Enabled"
}

variable "index_document_location" {
  description = "The location/path of the index.html document in the bucket"
  type        = string
  default     = "index.html"
}

variable "error_document_location" {
  description = "The location/path of the error.html document in the bucket"
  type        = string
  default     = "index.html"
}

variable "cloudfront_origin_access_identity_iam_arn" {
  description = "The ARN of the CloudFront Origin Access Identity"
  type        = string
}
