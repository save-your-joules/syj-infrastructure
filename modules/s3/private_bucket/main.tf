# S3 Private Bucket

##################
# --- Locals --- #
##################

###################
# --- Modules --- #
###################

#####################
# --- Resources --- #
#####################
# --- S3 ----------------------------------------------------
resource "random_pet" "this" {
  separator = "-"
  length    = 2
}

resource "aws_s3_bucket" "this" {
  bucket = var.append_random_string ? "${lower(replace(var.bucket_name, "_", "-"))}-${random_pet.this.id}" : lower(replace(var.bucket_name, "_", "-"))
}

resource "aws_s3_bucket_acl" "frontend" {
  bucket = aws_s3_bucket.this.id
  acl    = "private"
}

resource "aws_s3_bucket_public_access_block" "frontend" {
  bucket = aws_s3_bucket.this.id

  block_public_acls       = true
  block_public_policy     = true
  restrict_public_buckets = true
  ignore_public_acls      = true
}

resource "aws_s3_bucket_versioning" "frontend" {
  bucket = aws_s3_bucket.this.id
  versioning_configuration {
    status = var.versioning
  }
}

resource "aws_s3_bucket_policy" "frontend" {
  count  = var.bucket_policy_json == null ? 0 : 1
  bucket = aws_s3_bucket.this.id
  policy = var.bucket_policy_json
}