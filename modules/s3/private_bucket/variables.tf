variable "bucket_name" {
  type        = string
  description = "The name to give the S3 bucket (will be appended with a random pet x2"
}

variable "versioning" {
  type        = string
  description = "Whether or not to enable versioning [Enabled, Suspended]"
}

variable "append_random_string" {
  type        = bool
  description = "Adds a random two word string to the bucket name"
  default     = true
}

variable "bucket_policy_json" {
  type        = string
  description = "The JSON encoded string of a policy to apply to the bucket"
  default     = null
}