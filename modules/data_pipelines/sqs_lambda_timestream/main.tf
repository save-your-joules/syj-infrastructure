# Data Pipeline - SQS Lambda Timestream Module
# Use this module to set up a data collector using a lambda that pulls events from an SQS queue, pulls the relevant data
# from the source and places into a timestream database
data "aws_caller_identity" "this" {}
data "aws_region" "this" {}

##################
# --- Locals --- #
##################
locals {
  component_name = var.data_source_parameters.ShortName
  lambda_env_vars = {
    timestream_database_name = var.timestream_db_name
    timestream_table_name    = aws_timestreamwrite_table.this.table_name
  }
}

###################
# --- Modules --- #
###################
module "sqs_consumer_lambda" {
  source                = "../../lambda/function"
  function_name         = "${var.resource_name_prefix}_${local.component_name}_sqs_consumer_function"
  resource_name_prefix  = var.resource_name_prefix
  source_file_path      = "/resources/lambda_source_code/functions/data_fetch/${var.lambda_function_folder_name}"
  output_zip_path       = "/resources/lambda_source_code/functions/data_fetch/${var.lambda_function_folder_name}.zip"
  parent_log_group_name = aws_cloudwatch_log_group.this.name
  handler               = "lambda_function.lambda_handler"
  layer_arns            = var.lambda_layer_arns
  create_role           = true
  max_execution_time    = 120
  iam_policy_arn        = aws_iam_policy.sqs_consumer.arn
  environment_variables = local.lambda_env_vars
  tags                  = var.tags
}

module "timestream_error_logs_bucket" {
  source               = "../../s3/private_bucket"
  bucket_name          = "${var.resource_name_prefix}_${local.component_name}_timestream_error_logs"
  append_random_string = false
  versioning           = "Suspended"
}


#####################
# --- Resources --- #
#####################
# --- Lambda --------------------------------------------------------
resource "aws_lambda_event_source_mapping" "sqs_consumer" {
  event_source_arn                   = aws_sqs_queue.data_collection.arn
  function_name                      = module.sqs_consumer_lambda.function_arn
  maximum_batching_window_in_seconds = 60
}

# --- IAM -----------------------------------------------------------
data "aws_iam_policy_document" "sqs_consumer" {
  statement {
    sid    = "CWLogging"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["arn:aws:logs:*:*:*"]
  }
  statement {
    sid    = "SQSConsume"
    effect = "Allow"
    actions = [
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage",
      "sqs:GetQueueAttributes"
    ]
    resources = [aws_sqs_queue.data_collection.arn, aws_sqs_queue.data_collection_dql.arn]
  }
  statement {
    sid    = "WriteToTimestream"
    effect = "Allow"
    actions = [
      "timestream:WriteRecords"
    ]
    resources = [aws_timestreamwrite_table.this.arn]
  }
  statement {
    sid    = "DescribeTimestreamEndpoints"
    effect = "Allow"
    actions = [
      "timestream:DescribeEndpoints"
    ]
    resources = ["*"]
  }
  dynamic "statement" {
    for_each = aws_secretsmanager_secret_version.this
    content {
      sid       = "AllowSecretRead"
      effect    = "Allow"
      actions   = ["secretsmanager:GetSecretValue"]
      resources = [statement.value.arn]
    }
  }
}

resource "aws_iam_policy" "sqs_consumer" {
  name   = "${var.resource_name_prefix}_${local.component_name}_sqs_consumer_function_policy"
  policy = data.aws_iam_policy_document.sqs_consumer.json
}

# --- Timestream ----------------------------------------------------
resource "aws_timestreamwrite_table" "this" {
  database_name = var.timestream_db_name
  table_name    = "${var.resource_name_prefix}_${local.component_name}_records"
  magnetic_store_write_properties {
    enable_magnetic_store_writes = true
    magnetic_store_rejected_data_location {
      s3_configuration {
        bucket_name       = module.timestream_error_logs_bucket.bucket_name
        object_key_prefix = local.component_name
      }
    }
  }
  retention_properties {
    magnetic_store_retention_period_in_days = 73000
    memory_store_retention_period_in_hours  = 1
  }
  tags = var.tags
}

# --- SQS -----------------------------------------------------------
resource "aws_sqs_queue" "data_collection" {
  name                       = "${var.resource_name_prefix}_${local.component_name}_queue"
  delay_seconds              = 0
  visibility_timeout_seconds = 3600 # 60 Minutes, this should be 6x than the Lambda timeout
  message_retention_seconds  = 345600
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.data_collection_dql.arn
    maxReceiveCount     = 4
  })
  redrive_allow_policy = jsonencode({
    redrivePermission = "byQueue",
    sourceQueueArns   = [aws_sqs_queue.data_collection_dql.arn]
  })
  tags = var.tags
}

resource "aws_sqs_queue" "data_collection_dql" {
  name                       = "${var.resource_name_prefix}_${local.component_name}_dlq"
  delay_seconds              = 0
  visibility_timeout_seconds = 660 # 11 Minutes, this should be longer than the Lambda timeout
  message_retention_seconds  = 345600
  tags                       = var.tags
}

resource "aws_sqs_queue_policy" "data_collection" {
  queue_url = aws_sqs_queue.data_collection.url

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "sqspolicy",
  "Statement": [
    {
      "Sid": "First",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "sqs:SendMessage",
      "Resource": "${aws_sqs_queue.data_collection.arn}",
      "Condition": {
        "ArnLike": {
            "aws:SourceArn": "arn:aws:events:${data.aws_region.this.name}:${data.aws_caller_identity.this.account_id}:rule/${var.resource_name_prefix}_${local.component_name}_*"
        }
      }
    }
  ]
}
POLICY
}

# --- CloudWatch ----------------------------------------------------
resource "aws_cloudwatch_log_group" "this" {
  name              = "${var.resource_name_prefix}/${local.component_name}"
  retention_in_days = var.cloudwatch_logs_retention_days
  tags              = var.tags
}

resource "aws_cloudwatch_event_rule" "data_collection_schedule" {
  name                = "${var.resource_name_prefix}_${local.component_name}_${local.component_name}"
  description         = var.data_source_parameters.Description
  schedule_expression = "cron(${var.data_source_parameters.CronSchedule})"
  tags                = var.tags
}

resource "aws_cloudwatch_event_target" "sqs" {
  rule      = aws_cloudwatch_event_rule.data_collection_schedule.name
  target_id = "SendToSQS"
  input = jsonencode(
    {
      Name                 = local.component_name,
      Description          = var.data_source_parameters.Description,
      CronSchedule         = var.data_source_parameters.CronSchedule,
      Url                  = var.data_source_parameters.Url,
      HTTPMethod           = var.data_source_parameters.HTTPMethod,
      ResponseFormat       = var.data_source_parameters.ResponseFormat,
      RequestBody          = var.data_source_parameters.RequestBody,
      SelectionExpression  = var.data_source_parameters.SelectionExpression,
      TimestreamDimensions = var.data_source_parameters.TimestreamDimensions,
      SecretNeeded         = var.data_source_parameters.SecretNeeded
  })
  arn = aws_sqs_queue.data_collection.arn
}

# --- Secrets Manager -----------------------------------------------
resource "aws_secretsmanager_secret" "this" {
  name  = "${local.component_name}_api_key"
  count = var.data_source_parameters.SecretNeeded == true ? 1 : 0
}

resource "aws_secretsmanager_secret_version" "this" {
  count     = var.data_source_parameters.SecretNeeded == true ? 1 : 0
  secret_id = aws_secretsmanager_secret_version.this[0].id
}
