variable "resource_name_prefix" {
  type        = string
  description = "The name to prefix to all created resources."
}

variable "tags" {
  type        = object({})
  default     = {}
  description = "The set of tags required for the resources."
}

variable "cloudwatch_logs_retention_days" {
  type        = number
  description = "The amount of days to keep cloudwatch logs"
}

variable "timestream_db_name" {
  description = "The name of the timestream database to create tables in"
  type        = string
}

variable "data_source_name" {
  description = "The name of the data source"
  type        = string
}

variable "lambda_function_folder_name" {
  description = "The folder name of the lambda function that will fetch and process the data"
  type        = string
}

variable "data_source_parameters" {
  description = "A map of parameters relating to the data source"
  type = object({
    ShortName            = string
    Description          = string
    CronSchedule         = string
    Url                  = string
    HTTPMethod           = string
    ResponseFormat       = string
    RequestBody          = string
    SelectionExpression  = string
    TimestreamDimensions = list(string)
    SecretNeeded         = string
  })
}

variable "lambda_layer_arns" {
  type        = list(string)
  description = "A list of all the Lambda layers to add to the lambda function"
}