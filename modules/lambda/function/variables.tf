variable "resource_name_prefix" {
  type        = string
  description = "The name to prefix to all created resources."
}

variable "tags" {
  type        = object({})
  description = "The set of tags required for the resources."
  default     = {}
}

variable "kms_key_arn" {
  description = "The ARN of the master KMS key"
  type        = string
  default     = null
}

variable "create_role" {
  description = "Whether or not to automatically create an IAM role for the function"
  type        = bool
  default     = false
}

variable "create_policy" {
  description = "Whether or not to automatically create an IAM policy for the function"
  type        = bool
  default     = false
}

variable "iam_role_arn" {
  description = "The ARN of the IAM role to attach to the Lambda function"
  type        = string
  default     = null
}

variable "iam_policy_arn" {
  description = "The ARN of the IAM policy to attach to the Lambda function"
  type        = string
  default     = null
}

variable "function_name" {
  description = "The name to use for the Lambda function"
  type        = string
}

variable "layer_arns" {
  description = "A list of lambda layer arns to attach to the function"
  type        = list(string)
}

variable "log_retention_in_days" {
  description = "The amount of days to keep the Lambda function logs"
  type        = string
  default     = 14
}

variable "handler" {
  description = "The name of the handler function"
  type        = string
  default     = "lambda_function.lambda_handler"
}

variable "parent_log_group_name" {
  type        = string
  description = "The name of the parent log group"
}

variable "runtime" {
  description = "The chosen runtime for the function"
  default     = "python3.9"
  type        = string
}

variable "max_execution_time" {
  type        = number
  description = "Maximum amount of time the function can run in seconds"
  default     = 15
}

variable "environment_variables" {
  description = "Environment variables to expose to the Lambda function"
  default     = { foo = "bar" }
  type        = map(string)
}

variable "source_file_path" {
  description = "The path to the Lambda function source code"
  type        = string
}

variable "output_zip_path" {
  description = "The path in which to place the archive/zip file of the function"
  type        = string
}

