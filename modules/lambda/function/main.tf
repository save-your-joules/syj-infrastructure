#########################
# --- Lambda module --- #
#########################

data "archive_file" "this" {
  type        = "zip"
  source_dir  = "${path.root}${var.source_file_path}"
  output_path = "${path.root}${var.output_zip_path}"
}

resource "aws_lambda_function" "this" {
  function_name    = var.function_name
  role             = var.iam_role_arn != null ? var.iam_role_arn : aws_iam_role.this[0].arn
  kms_key_arn      = var.kms_key_arn
  filename         = data.archive_file.this.output_path
  handler          = var.handler
  source_code_hash = filebase64sha256(data.archive_file.this.output_path)
  timeout          = var.max_execution_time

  layers  = var.layer_arns
  runtime = var.runtime

  environment {
    variables = var.environment_variables
  }
}

resource "aws_iam_role" "this" {
  count              = var.create_role ? 1 : 0
  name               = "${var.function_name}_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": "LambdaAssume"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "this" {
  count  = var.create_policy ? 1 : 0
  name   = "${var.function_name}_policy"
  policy = data.aws_iam_policy_document.this[count.index].json
}

resource "aws_iam_role_policy_attachment" "this" {
  count      = var.create_role ? 1 : 0
  role       = aws_iam_role.this[0].name
  policy_arn = var.iam_policy_arn != null ? var.iam_policy_arn : aws_iam_policy.this[0].arn
}

data "aws_iam_policy_document" "this" {
  count = var.create_role ? 1 : 0
  statement {
    sid    = "CWLogging"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["arn:aws:logs:*:*:*"]
  }
  dynamic "statement" {
    for_each = var.kms_key_arn != null ? ["allow_kms"] : []
    content {
      sid = "KMS"
      actions = [
        "kms:Decrypt",
        "kms:Encrypt",
        "kms:CreateGrant"
      ]
      resources = [
        var.kms_key_arn
      ]
    }
  }
  dynamic "statement" {
    for_each = var.kms_key_arn != null ? ["allow_kms"] : []
    content {
      sid = "KMS"
      actions = [
        "kms:ListAliases"
      ]
      resources = [
        "*"
      ]
    }
  }
}

resource "aws_cloudwatch_log_group" "this" {
  name              = "/aws/lambda/${aws_lambda_function.this.function_name}"
  retention_in_days = var.log_retention_in_days
  kms_key_id        = var.kms_key_arn
}