#   Save Your Joules
#
#   Description: This terraform project deploys the AWS infrastructure necessary to host the Save Your Joules
#
#   28/02/2022
#   Written by Samuel Kemp

##################
# --- Locals --- #
##################

###################
# --- Modules --- #
###################

module "frontend" {
  source               = "./resources/frontend"
  resource_name_prefix = var.resource_name_prefix
  domain_name          = var.domain_name
  region               = var.region
}

module "backend" {
  source                                 = "./resources/backend"
  resource_name_prefix                   = var.resource_name_prefix
  cloudwatch_logs_retention_days_default = var.cloudwatch_logs_retention_days_default
  environment                            = var.environment
}


#####################
# --- Resources --- #
#####################
#tfsec:ignore:aws-dynamodb-enable-recovery tfsec:ignore:aws-dynamodb-table-customer-key
resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  name           = "terraform-state-lock"
  billing_mode   = "PROVISIONED"
  hash_key       = "LockID"
  read_capacity  = 1
  write_capacity = 1

  attribute {
    name = "LockID"
    type = "S"
  }
}
