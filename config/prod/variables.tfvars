environment                            = "prod"
resource_name_prefix                   = "syj"
region                                 = "eu-west-1"
cloudwatch_logs_retention_days_default = 14

domain_name = "saveyourjoules.com"

