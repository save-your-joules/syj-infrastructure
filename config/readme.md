# Configuration Directory

The configuration directory contains the key files used to deploy Terraform. 

## Directories

### Development

The `config/dev` is used for the backend configuration and variables file for your local development. It should be included in `.gitignore` and not committed or pushed to version control.

### Preproduction

The `config/preprod` is used for deploying the Terraform configuration into a preproduction environment.

### Production

The `config/prod` should be used for deploying the Terraform configuration into a production environment. 

## Files

### Backend Configuration

The `backend.conf` file contains the key elements for the [Terraform Backend Configuration](https://www.terraform.io/docs/backends/config.html). Key components here should include:

* *Region* - the region that the backend configuration is stored in.
* *Key* - the key used to store the backend configuration. 

#### Development

In development, you may include other components. These are defined below. *Please note* none of these options should be defined if the Terraform configuration is running through a Nebula pipeline as they are defined upon execution by the pipeline itself.

* *Bucket* - the bucket that you store the state in.
* *Encrypt* - The encryption flag.
* *Key* - the key used to encrypt the Terraform state.

### Terraform Variables

The `variables.tfvars` file contains all of the variables for the specific environment. These are the main variables for the configuration.
