provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Terraform = true
      Project   = "syj"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  alias  = "us-east-1"
  default_tags {
    tags = {
      Terraform = true
      Project   = "syj"
    }
  }
}

terraform {
  required_version = "~> 1.00"
  backend "s3" {
    encrypt        = true
    dynamodb_table = "terraform-state-lock"
  }
}
