variable "resource_name_prefix" {
  type        = string
  description = "The name to prefix to all created resources."
}

variable "cloudwatch_logs_retention_days_default" {
  type        = string
  description = "The amount of days to keep cloudwatch logs by default"
}

variable "environment" {
  type        = string
  description = "The deployment environment name"
}