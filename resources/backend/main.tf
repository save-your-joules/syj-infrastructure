#   Backend

##################
# --- Locals --- #
##################

###################
# --- Modules --- #
###################
module "logging_monitoring" {
  source               = "./logging_monitoring"
  resource_name_prefix = var.resource_name_prefix
}

module "data_etl" {
  source                         = "./data_etl"
  resource_name_prefix           = var.resource_name_prefix
  timestream_db_name             = aws_timestreamwrite_database.this.database_name
  cloudwatch_logs_retention_days = var.cloudwatch_logs_retention_days_default
  lambda_layer_arns              = [aws_lambda_layer_version.etl_helper.arn]
  environment                    = var.environment
}

#####################
# --- Resources --- #
#####################
# --- TimeStream ----------------------------------------------------
resource "aws_timestreamwrite_database" "this" {
  database_name = "${var.resource_name_prefix}-time-series-data"
}

# --- Lambda --------------------------------------------------------
data "archive_file" "etl_helper_layer" {
  type        = "zip"
  source_dir  = "${path.root}/resources/lambda_source_code/layers/etl_helper"
  output_path = "${path.root}/resources/lambda_source_code/layers/etl_helper_layer.zip"
}

resource "aws_lambda_layer_version" "etl_helper" {
  layer_name               = "${var.resource_name_prefix}_etl_helper"
  description              = "A layer with libraries to help in fetching data from multiple sources"
  filename                 = data.archive_file.etl_helper_layer.output_path
  compatible_architectures = ["x86_64"]
  compatible_runtimes      = ["python3.8", "python3.9"]
  source_code_hash         = filebase64sha256(data.archive_file.etl_helper_layer.output_path)
}