variable "resource_name_prefix" {
  type        = string
  description = "The name to prefix to all created resources."
}

variable "cloudwatch_logs_retention_days" {
  type        = string
  description = "The amount of days to keep cloudwatch logs"
}

variable "timestream_db_name" {
  description = "The name of the timestream database to create tables in"
  type        = string
}

variable "lambda_layer_arns" {
  description = "A list of lambda layers to use"
  type        = list(string)
}

variable "environment" {
  type        = string
  description = "The deployment environment name"
}