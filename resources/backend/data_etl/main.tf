# Grid Live

##################
# --- Locals --- #
##################
locals {
  resource_name_prefix = "${var.resource_name_prefix}_grid_live"
  grid_source_list     = jsondecode(file("${path.root}/config/${var.environment}/data_sources/grid_sources.json"))
}


###################
# --- Modules --- #
###################
module "data_pipelines" {
  for_each = local.grid_source_list.Sources
  source   = "../../../modules/data_pipelines/sqs_lambda_timestream"

  cloudwatch_logs_retention_days = 14
  data_source_name               = each.key
  data_source_parameters         = each.value
  lambda_function_folder_name    = each.key
  lambda_layer_arns              = var.lambda_layer_arns
  resource_name_prefix           = local.resource_name_prefix
  timestream_db_name             = var.timestream_db_name
}


#####################
# --- Resources --- #
#####################


