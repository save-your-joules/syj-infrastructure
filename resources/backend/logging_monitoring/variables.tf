#####################
# --- Variables --- #
#####################
variable "resource_name_prefix" {
  type        = string
  description = "The prefix to give all resources"
}