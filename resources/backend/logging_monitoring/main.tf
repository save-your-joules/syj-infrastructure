#   All central logging and monitoring

###################
# --- Modules --- #
###################

#####################
# --- Resources --- #
#####################
# --- CloudWatch ----------------------------------------------------
resource "aws_cloudwatch_metric_alarm" "lambda" {
  alarm_name          = "${var.resource_name_prefix}_lambda_error_alarm"
  alarm_description   = "Will trigger on any Lambda function error"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  period              = 300
  statistic           = "Maximum"
  metric_name         = "Errors"
  namespace           = "AWS/Lambda"
  alarm_actions       = [aws_sns_topic.alarms.arn]
}

# --- SNS -----------------------------------------------------------
#tfsec:ignore:aws-sns-enable-topic-encryption
resource "aws_sns_topic" "alarms" {
  name = "${var.resource_name_prefix}_alarms"
}