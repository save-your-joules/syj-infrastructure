"""ETL helper classes and supporting functions
We use multiple different lambda functions to etl our data but a lot of them require the same helper functions which
are located here.
"""

####################
# -----Imports------
####################
import boto3
import botocore.exceptions
import logging
import time
import json
import urllib3
import xmltodict
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

logger = logging.getLogger(__name__)
logger.setLevel("INFO")

http = urllib3.PoolManager()


#####################
# -----Functions-----
#####################
def write_to_timestream(common_attributes: dict, records: list, database_name: str, table_name: str):
    """
    Write list of records to AWS Timestream

    :param common_attributes:
    :param records: List of records to write
    :param database_name: Name of the TimeStream Database
    :param table_name: Name of the TimeStream table
    :return:
    """

    ts = boto3.client('timestream-write')

    try:
        response = ts.write_records(
            DatabaseName=database_name,
            TableName=table_name,
            CommonAttributes=common_attributes,
            Records=records
        )
    except ts.exceptions.RejectedRecordsException as err:
        logging.error(f"RejectedRecords: {err}")
        for rr in err.response["RejectedRecords"]:
            print("Rejected Index " + str(rr["RecordIndex"]) + ": " + rr["Reason"])
        print("Other records were written successfully. ")
    except botocore.exceptions.ClientError as e:
        logging.error(f"There was a Client Error when attempting to write data to TimeStream: {e}")
        raise
    except Exception as e:
        logging.error(f"There was a Generic Error when attempting to write data to TimeStream: {e}")
        raise

    return None


def write_multi_to_timestream(common_attributes: dict, records: list, database_name: str, table_name: str):
    """
    Write list of records to AWS Timestream

    :param common_attributes:
    :param records: List of records to write
    :param database_name: Name of the TimeStream Database
    :param table_name: Name of the TimeStream table
    :return:
    """

    ts = boto3.client('timestream-write')
    try:
        response = ts.write_records(
            DatabaseName=database_name,
            TableName=table_name,
            CommonAttributes=common_attributes,
            Records=records
        )
        logger.debug(f"Response:{response}")
    except ts.exceptions.RejectedRecordsException as err:
        logging.error(f"RejectedRecords: {err}")
        for rr in err.response["RejectedRecords"]:
            print("Rejected Index " + str(rr["RecordIndex"]) + ": " + rr["Reason"])
        print("Other records were written successfully. ")
    except botocore.exceptions.ClientError as err:
        logging.error(f"There was a Client Error when attempting to write data to TimeStream: {err}")
        raise Exception
    except Exception as err:
        logging.error(f"There was a Generic Error when attempting to write data to TimeStream: {err}")
        raise Exception

    return None


def retrieve_data(url, http_method, response_format, api_key=None, request_body=None):
    """
    Retrieve data from a specific URL and filter response based on a selection expression

    :param url:
    :param http_method:
    :param api_key:
    :param response_format:
    :param request_body:
    :return:
    """

    headers = {
        'Content-Type': response_format,
        'Accept': response_format,
        'Accept-Encoding': 'gzip, deflate'
    }

    if http_method.upper() == 'GET':
        try:
            r = requests.get(url, timeout=(10, 120), headers=headers)
            return r.content
        except TimeoutError:
            logging.error(f"The connection timed out when trying to retrieve data from {url}")

    elif http_method.upper() == 'POST':
        try:
            r = requests.post(url, data=request_body, timeout=(10, 60), headers=headers)
            return r.content
        except TimeoutError:
            logging.error(f"The connection timed out when trying to retrieve data from {url}")


def get_secret(secret_name):
    """
    Fetch a secret from AWS Secrets Manager

    :param secret_name:
    :return:
    """
    sm = boto3.client('secretsmanager')
    try:
        response = sm.get_secret_value(SecretId=secret_name)
        secret_string = response['SecretString']
    except botocore.exceptions.ClientError as e:
        logging.error("There was an error retrieving the Secret from SecretsManager")
        raise Exception(e)
    except KeyError:
        logging.error("There was a Key Error retrieving the Secret from SecretsManager")
        raise Exception

    return response['SecretString']


def build_multi_measure(data, names_types, measure_name):
    measure_values = []
    for key in names_types.keys():
        value = data[key]
        value_type = names_types[key]
        # Deal with null or nan values
        if value is None or value == '':
            logger.info(
                f"Value of 'None' received for {key}; "
                "not writing measure to Timestream"
            )
        else:
            measure_values.append(
                {
                    'Name': key,
                    'Value': str(value),
                    'Type': value_type
                }
            )

    return {
        'MeasureName': measure_name,
        'MeasureValueType': 'MULTI',
        'MeasureValues': measure_values
    }


def build_measure(measure_name, combined_dict, measure_type):
    """Builds a measure object ready to write to timestream

    :param measure_name: Name of the measure
    :param combined_dict: A dict containing key value pairs of the measure and corresponding value
    :param measure_type: The data type of the measure value
    :return:
    """
    value = combined_dict[measure_name]
    measure = None
    if value is None and value != '':

        measure = {
            'MeasureName': measure_name,
            'MeasureValue': str(value),
            'MeasureValueType': measure_type
        }


    return measure


def xml_parser(data, selection_expression):
    """
    Parse XML formatted string
    :param data:
    :param selection_expression:
    :return:
    """
    parsed_data = xmltodict.parse(data)
    selection_expression_dict = xmltodict.parse(selection_expression)
    selected_items = select_from_dict(parsed_data, selection_expression_dict)
    flattened_list = flatten(selected_items)
    return flattened_list


def find_by_key(data, target):
    """
    Find values by key in a nested dict

    :param data:
    :param target:
    :return:
    """
    for key, value in data.items():
        if key == target:
            yield value
        elif isinstance(value, dict):
            yield from find_by_key(value, target)


def select_from_dict(dictionary, selection_expression, selected_items=[]):
    """
    Select values from a dict based on another dict/selection expression

    :param dictionary:
    :param selection_expression:
    :param selected_items:
    :return:
    """
    if selection_expression is not None:
        for y in selection_expression:
            new_selection = selection_expression[y]
            if not isinstance(new_selection, dict):
                for x in find_by_key(dictionary, y):
                    selected_items.append(x)
                    continue
            for new_dict in find_by_key(dictionary, y):
                selected_items = select_from_dict(new_dict, new_selection, selected_items)
    return selected_items


def flatten(list_of_lists):
    """
    Flatten a 2-D list to a 1-D list

    :param list_of_lists:
    :return:
    """
    if len(list_of_lists) == 0:
        return list_of_lists
    if isinstance(list_of_lists[0], list):
        return flatten(list_of_lists[0]) + flatten(list_of_lists[1:])
    return list_of_lists[:1] + flatten(list_of_lists[1:])



