import json
import time
import boto3
import pytest
from python import helper_functions as help

from moto import mock_timestreamwrite
from moto.core import DEFAULT_ACCOUNT_ID as ACCOUNT_ID
from moto.timestreamwrite.models import timestreamwrite_backends


TIMESTREAM_DATABASE_NAME = 'syj-time-series-data'
TIMESTREAM_TABLE_NAME = 'syj_grid_live_ShefSolPVLive_NatAgg_records'

t_epoch = str(int(round(time.time() * 1000)))


@pytest.fixture
def nighttime_contract():
    input = b'{"data": [[0, "2023-03-11T19:00:00Z", 0, null, 12994.386, 13861.203, 0, 0, 0, null, 880]], "meta": ["gsp_id", "datetime_gmt", "generation_mw", "bias_error", "capacity_mwp", "installedcapacity_mwp", "lcl_mw", "stats_error", "ucl_mw", "uncertainty_MW", "site_count"]}'
    output = [
        {
            "MeasureName": "nationally_aggregated_metrics",
            "MeasureValueType": "MULTI",
            "MeasureValues": [
                {"Name": "datetime_gmt", "Type": "VARCHAR", "Value": "2023-03-11T19:00:00Z"},
                {"Name": "generation_mw", "Type": "DOUBLE", "Value": "0"},
                {"Name": "capacity_mwp", "Type": "DOUBLE", "Value": "12994.386"},
                {"Name": "installedcapacity_mwp", "Type": "DOUBLE", "Value": "13861.203"},
                {"Name": "lcl_mw", "Type": "DOUBLE", "Value": "0"},
                {"Name": "stats_error", "Type": "DOUBLE", "Value": "0"},
                {"Name": "ucl_mw", "Type": "DOUBLE", "Value": "0"},
                {"Name": "site_count", "Type": "BIGINT", "Value": "880"},
            ],
        },
    ]
    yield (input, output)


@pytest.fixture
def daytime_contract():
    input = b'{"data": [[0, "2023-03-11T14:00:00Z", 3077.57, null, 12994.386, 13861.203, 2980.7, 70.6016, 3174.45, null, 1361]], "meta": ["gsp_id", "datetime_gmt", "generation_mw", "bias_error", "capacity_mwp", "installedcapacity_mwp", "lcl_mw", "stats_error", "ucl_mw", "uncertainty_MW", "site_count"]}'
    output = [
        {
            "MeasureName": "nationally_aggregated_metrics",
            "MeasureValueType": "MULTI",
            "MeasureValues": [
                {"Name": "datetime_gmt", "Type": "VARCHAR", "Value": "2023-03-11T14:00:00Z"},
                {"Name": "generation_mw", "Type": "DOUBLE", "Value": "3077.57"},
                {"Name": "capacity_mwp", "Type": "DOUBLE", "Value": "12994.386"},
                {"Name": "installedcapacity_mwp", "Type": "DOUBLE", "Value": "13861.203"},
                {"Name": "lcl_mw", "Type": "DOUBLE", "Value": "2980.7"},
                {"Name": "stats_error", "Type": "DOUBLE", "Value": "70.6016"},
                {"Name": "ucl_mw", "Type": "DOUBLE", "Value": "3174.45"},
                {"Name": "site_count", "Type": "BIGINT", "Value": "1361"},
            ],
        },
    ]
    yield (input, output)


@mock_timestreamwrite
def test_build_valid(daytime_contract):
    client = boto3.client('timestream-write', region_name="eu-west-1")
    _ = client.create_database(
        DatabaseName=TIMESTREAM_DATABASE_NAME,
    )
    response = client.create_table(
        DatabaseName=TIMESTREAM_DATABASE_NAME,
        TableName=TIMESTREAM_TABLE_NAME
    )

    measure_name = "nationally_aggregated_metrics"
    dimensions = ["gsp_id"]

    data = json.loads(daytime_contract[0])
    meta = data["meta"]
    data = data["data"][0]
    combined_dict = dict(zip(meta, data))

    configured_dimensions = []
    for dimension in dimensions:
        configured_dimensions.append({
            'Name': dimension,
            'Value': str(combined_dict[dimension])
        })
    common_attributes = {
        'Dimensions': configured_dimensions,
        'Time': t_epoch
    }

    names = [
        "datetime_gmt",
        "generation_mw",
        "bias_error",
        "capacity_mwp",
        "installedcapacity_mwp",
        "lcl_mw",
        "stats_error",
        "ucl_mw",
        "uncertainty_MW",
        "site_count",
    ]

    types = [
        "VARCHAR",
        "DOUBLE",
        "DOUBLE",
        "DOUBLE",
        "DOUBLE",
        "DOUBLE",
        "DOUBLE",
        "DOUBLE",
        "DOUBLE",
        "BIGINT",
    ]

    names_types = dict(zip(names, types))

    assert names_types == {
        "datetime_gmt": "VARCHAR",
        "generation_mw": "DOUBLE",
        "bias_error": "DOUBLE",
        "capacity_mwp": "DOUBLE",
        "installedcapacity_mwp": "DOUBLE",
        "lcl_mw": "DOUBLE",
        "stats_error": "DOUBLE",
        "ucl_mw": "DOUBLE",
        "uncertainty_MW": "DOUBLE",
        "site_count": "BIGINT",
    }

    records = [
        help.build_multi_measure(
            combined_dict,
            names_types,
            measure_name
        )
    ]

    assert records == daytime_contract[1]

    response = help.write_multi_to_timestream(
        common_attributes,
        records,
        TIMESTREAM_DATABASE_NAME,
        TIMESTREAM_TABLE_NAME
    )

    assert response is None

    backend = timestreamwrite_backends[ACCOUNT_ID]["eu-west-1"]
    tables = backend.databases[TIMESTREAM_DATABASE_NAME].tables
    assert tables[TIMESTREAM_TABLE_NAME].records == daytime_contract[1]
