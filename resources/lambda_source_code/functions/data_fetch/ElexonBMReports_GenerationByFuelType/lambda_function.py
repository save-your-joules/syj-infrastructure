"""
    A Lambda function to fetch data from HTTP or REST endpoints and write it an AWS Timestream database

    runtime: Python3.9
"""

import logging
import json
import time
from dateutil.parser import isoparse
import helper_functions as help
import os
import boto3
import botocore.exceptions

# Logging
logger = logging.getLogger(__name__)
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.INFO)


# Environment Variables
TIMESTREAM_DATABASE_NAME = os.environ['timestream_database_name']
TIMESTREAM_TABLE_NAME = os.environ['timestream_table_name']

# Time
t_epoch = str(int(round(time.time() * 1000)))


def lambda_handler(event, context):
    messages_to_reprocess = []
    batch_failure_response = {}
    logger.debug(f"Event: {event}")

    for record in event['Records']:
        try:
            body = json.loads(record['body'])

            source_name = body['Name']
            url = body['Url']
            http_method = body['HTTPMethod']
            request_body = body['RequestBody']
            response_format = body['ResponseFormat']
            selection_expression = body['SelectionExpression']
            dimensions = body['TimestreamDimensions']
            secret_needed = body['SecretNeeded']

            if "true" in secret_needed:
                secret_name = source_name + "_api_key"
                secret_string = help.get_secret(secret_name)
                xml = help.retrieve_data(url, http_method, secret_string, response_format, request_body)
            else:
                xml = help.retrieve_data(url, http_method, response_format, request_body)

            data = help.xml_parser(xml, selection_expression)
            logger.info(f"Parsed data: {data}")
            # Create records for the individual fuel generations
            for item in data:
                if "currentTotalMW" in item:
                    # Create records that relate to the total generation
                    common_attributes, records = create_total_record(item, "metrics")
                    logger.debug(f"Total Record Common Attributes: {common_attributes}")
                    logger.debug(f"Total Record: {records}")
                    help.write_multi_to_timestream(common_attributes, records, TIMESTREAM_DATABASE_NAME, TIMESTREAM_TABLE_NAME)

                if "fuelType" in item:
                    common_attributes, records = create_records(item, dimensions, "metrics")
                    logger.debug(f"Fuel Type Record Common Attributes: {common_attributes}")
                    logger.debug(f"Fuel Type Records: {records}")
                    help.write_multi_to_timestream(common_attributes, records, TIMESTREAM_DATABASE_NAME, TIMESTREAM_TABLE_NAME)

                elif "biddingZone" in item:
                    common_attributes, records = create_records(item, ["recordType", "biddingZone"], "metrics")
                    logger.debug(f"Bidding Zone Record Common Attributes: {common_attributes}")
                    logger.debug(f"Bidding Zone Records: {records}")
                    help.write_multi_to_timestream(common_attributes, records, TIMESTREAM_DATABASE_NAME, TIMESTREAM_TABLE_NAME)

            logger.info(f"{len(data)} records processed successfully")
        except Exception as e:
            logger.error(f"Error: {e}")
            messages_to_reprocess.append({"itemIdentifier": record['messageId']})

    batch_failure_response["batchItemFailures"] = messages_to_reprocess
    return batch_failure_response


def create_records(data, dimensions, measure_name):

    configured_dimensions = []
    for dimension in dimensions:
        configured_dimensions.append({
            'Name': dimension,
            'Value': str(data[dimension])
        })
    common_attributes = {
        'Dimensions': configured_dimensions,
        'Time': t_epoch  # this needs to be changed as it causes non duplicates to be treated as duplicates
    }

    names = ['currentMW',
             'currentPercentage',
             'lastHalfHourLocalStartTime',
             'lastHalfHourLocalEndTime',
             'lastHalfHourMW',
             'lastHalfHourPercentage',
             'last24HourLocalStartTime',
             'last24HourLocalEndTime',
             'last24HourMWh',
             'last24HourPercentage',
             'activeFlag'
             ]
    types = ['DOUBLE',
             'DOUBLE',
             'VARCHAR',
             'VARCHAR',
             'DOUBLE',
             'DOUBLE',
             'VARCHAR',
             'VARCHAR',
             'DOUBLE',
             'DOUBLE',
             'VARCHAR'
             ]

    names_types = dict(zip(names, types))
    records = [help.build_multi_measure(data, names_types, measure_name)]

    return common_attributes, records


def create_total_record(data, measure_name):

    configured_dimensions = [
        {
            'Name': "recordType",
            'Value': "FUELINSTHHCUR"
        },
        {
            'Name': "fuelType",
            'Value': "Total"
        }
    ]

    common_attributes = {
        'Dimensions': configured_dimensions,
        'Time': t_epoch
    }

    names = ['currentTotalMW',
             'currentTotalPercentage',
             'lastHalfHourTotalMW',
             'lastHalfHourTotalpercentage',
             'last24HourTotalMWh',
             'last24HourTotalPercentage',

             ]
    types = ['DOUBLE',
             'DOUBLE',
             'DOUBLE',
             'DOUBLE',
             'DOUBLE',
             'DOUBLE'
             ]
    names_types = dict(zip(names, types))
    records = [help.build_multi_measure(data, names_types, measure_name)]

    return common_attributes, records


