"""
    A Lambda function to fetch data from HTTP or REST endpoints and write it an AWS Timestream database

    runtime: Python3.9
"""

import logging
import json
import time
from dateutil.parser import isoparse
import helper_functions as help
import os
import boto3
import botocore.exceptions

# Logging
logger = logging.getLogger(__name__)
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)


# Environment Variables
TIMESTREAM_DATABASE_NAME = os.environ['timestream_database_name']
TIMESTREAM_TABLE_NAME = os.environ['timestream_table_name']

# Time
t_epoch = str(int(round(time.time() * 1000)))


def lambda_handler(event, context):
    messages_to_reprocess = []
    batch_failure_response = {}
    logger.info(f"Event: {event}")
    for record in event['Records']:
        try:
            body = json.loads(record['body'])

            source_name = body['Name']
            url = body['Url']
            http_method = body['HTTPMethod']
            request_body = body['RequestBody']
            response_format = body['ResponseFormat']
            selection_expression = body['SelectionExpression']
            dimensions = body['TimestreamDimensions']
            secret_needed = body['SecretNeeded']

            if "true" in secret_needed:
                secret_name = source_name + "_api_key"
                secret_string = help.get_secret(secret_name)
                data = help.retrieve_data(url, http_method, secret_string, response_format, request_body)
            else:
                data = help.retrieve_data(url, http_method, response_format, request_body)

            logger.debug(f"Parsed data: {data}")
            common_attributes, records = create_records(data, dimensions, "nationally_aggregated_metrics")
            logger.debug(f"Solar Record Common Attributes: {common_attributes}")
            logger.debug(f"Solar Records: {records}")
            help.write_multi_to_timestream(common_attributes, records, TIMESTREAM_DATABASE_NAME, TIMESTREAM_TABLE_NAME)

        except Exception as e:
            logger.error(f"Error: {e}")
            messages_to_reprocess.append({"itemIdentifier": record['messageId']})

    batch_failure_response["batchItemFailures"] = messages_to_reprocess
    return batch_failure_response


def create_records(data, dimensions, measure_name):
    data = json.loads(data)
    meta = data['meta']
    data = data['data'][0]
    combined_dict = dict(zip(meta, data))

    configured_dimensions = []
    for dimension in dimensions:
        configured_dimensions.append({
            'Name': dimension,
            'Value': str(combined_dict[dimension])
        })
    common_attributes = {
        'Dimensions': configured_dimensions,
        'Time': t_epoch
    }
    names = ['datetime_gmt',
             'generation_mw',
             'bias_error',
             'capacity_mwp',
             'installedcapacity_mwp',
             'lcl_mw',
             'stats_error',
             'ucl_mw',
             'uncertainty_MW',
             'site_count']

    types = ['VARCHAR',
             'DOUBLE',
             'DOUBLE',
             'DOUBLE',
             'DOUBLE',
             'DOUBLE',
             'DOUBLE',
             'DOUBLE',
             'DOUBLE',
             'BIGINT'
             ]

    names_types = dict(zip(names, types))
    records = [help.build_multi_measure(combined_dict, names_types, measure_name)]

    return common_attributes, records



