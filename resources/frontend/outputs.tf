output "front_end_bucket_arn" {
  value       = module.frontend_bucket.bucket_arn
  description = "The ARN of the S3 bucket that contains the files for the frontend website"
}