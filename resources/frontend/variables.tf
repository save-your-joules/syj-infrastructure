variable "resource_name_prefix" {
  type        = string
  description = "The name to prefix to all created resources."
}

variable "domain_name" {
  type        = string
  description = "Domain name of the website/service"
}

variable "region" {
  type        = string
  description = "The default region"
}
